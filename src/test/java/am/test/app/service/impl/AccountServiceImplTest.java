package am.test.app.service.impl;


import am.test.app.dao.AccountRepository;
import am.test.app.dto.AccountDto;
import am.test.app.entity.Account;
import am.test.app.exception.UserException;
import am.test.app.generate.AccountGenerator;
import am.test.app.mapper.AccountMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AccountServiceImplTest {

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private AccountMapper accountMapper;

    @InjectMocks
    private AccountServiceImpl accountService;

    private Account account;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        account = new Account();
    }

    @Test
    public void createUserShouldReturnTrue() throws UserException {
        //GIVEN
        AccountDto accountDto = AccountGenerator.applyValidAccountDto();
        //WHEN
        Account user = accountService.createUser(accountDto);
        //THEN
        verify(accountRepository).save(user);
    }

    @Test
    public void createUserShouldThrowUserException() throws UserException {
        //GIVEN
        AccountDto accountDto = AccountGenerator.applyInValidNullAccountDto();
        //WHEN
        Throwable throwable = catchThrowable(() -> accountService.createUser(accountDto));
        // THEN
        assertThat(throwable).isNotNull();
        assertThat(throwable.getMessage()).isEqualTo("UserIsNull");
    }

    @Test
    public void updateAccountMethodShouldUpdateAccount() throws UserException {
        //GIVEN
        account.setId(5L);
        AccountDto accountDto = AccountGenerator.applyValidAccountDto();
        when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        //WHEN
        accountService.updateAccount(account.getId(), accountDto);
        //THEN
        verify(accountRepository).save(accountMapper.toModel(accountDto));
    }

    @Test
    public void deleteAccountMethodShouldDeleteAccount() throws UserException {
        //GIVEN
        account.setId(4L);
        when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        when(accountRepository.deleteAccountById(account.getId())).thenReturn(Optional.of(account));
        //WHEN
        accountService.deleteAccount(account.getId());
        //THEN
        verify(accountRepository).deleteAccountById(account.getId());
    }

    @Test
    public void getAccountMethodShouldReturnAccount() throws UserException {
        //GIVEN
        account.setId(3L);
        when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        //WHEN
        accountService.getAccount(account.getId());
        //THEN
        verify(accountRepository).findById(account.getId());
    }

    @Test
    public void getAllAccountsMethodShouldReturnAllOfAccounts() throws UserException {
        //GIVEN
        List<Account> accounts = new ArrayList<>();
        when(accountRepository.findAll()).thenReturn(accounts);
        //WHEN
        accountService.getAllAccounts();
        //THEN
        verify(accountRepository).findAll();
    }
}