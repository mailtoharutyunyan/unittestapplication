package am.test.app.generate;

import am.test.app.dto.AccountDto;
import am.test.app.entity.Account;

public final class AccountGenerator {

    public static Account applyValidAccount() {
        return Account.builder()
                .id(1L)
                .email("a@mail.ru")
                .userName("username")
                .password("password")
                .mobileType("samsung")
                .build();
    }

    public static Account applyInValidAccount() {
        return Account.builder()
                .id(null)
                .email(null)
                .userName(null)
                .password(null)
                .mobileType(null)
                .build();
    }

    public static AccountDto applyValidAccountDto() {
        return AccountDto
                .builder()
                .email("test@mail.ru")
                .userName("myUsername")
                .password("password")
                .mobileType("Iphone")
                .build();
    }

    public static AccountDto applyInValidAccountDto() {
        return AccountDto
                .builder()
                .email(null)
                .userName(null)
                .password(null)
                .mobileType(null)
                .build();
    }

    public static AccountDto applyInValidNullAccountDto() {
        return null;
    }

}