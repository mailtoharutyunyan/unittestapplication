package am.test.app.mapper;

import am.test.app.dto.AccountDto;
import am.test.app.entity.Account;
import org.springframework.stereotype.Component;

@Component
public class AccountMapper {

    public Account toModel(AccountDto accountRequestDto) {
        return Account
                .builder()
                .email(accountRequestDto.getEmail())
                .userName(accountRequestDto.getUserName())
                .password(accountRequestDto.getPassword())
                .mobileType(accountRequestDto.getMobileType())
                .build();
    }

    public AccountDto toDto(Account account) {
        return AccountDto.builder()
                .email(account.getEmail())
                .userName(account.getUserName())
                .password(account.getPassword())
                .mobileType(account.getMobileType())
                .build();
    }
    //
}