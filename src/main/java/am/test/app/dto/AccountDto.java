package am.test.app.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Builder
public class AccountDto {
    private String email;
    private String userName;
    private String password;
    private String mobileType;
}
