package am.test.app.service;

import am.test.app.dto.AccountDto;
import am.test.app.entity.Account;
import am.test.app.exception.UserException;

import java.util.List;

public interface AccountService {

    Account createUser(AccountDto accountDto) throws UserException;

    Account updateAccount(Long id, AccountDto accountDto) throws UserException;

    Account deleteAccount(Long id) throws UserException;

    Account getAccount(Long id) throws UserException;

    List<Account> getAllAccounts() throws UserException;

}
