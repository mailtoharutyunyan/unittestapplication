package am.test.app.service.impl;

import am.test.app.dao.AccountRepository;
import am.test.app.dto.AccountDto;
import am.test.app.entity.Account;
import am.test.app.exception.UserException;
import am.test.app.mapper.AccountMapper;
import am.test.app.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;

    @Override
    public Account createUser(AccountDto accountDto) throws UserException {
        if (accountDto == null) {
            throw new UserException("UserIsNull");
        } else {
            return accountRepository.save(accountMapper.toModel(accountDto));
        }
    }

    @Override
    public Account updateAccount(Long id, AccountDto accountDto) throws UserException {
        Account account = accountRepository.findById(id)
                .orElseThrow(() -> new UserException("UserNotFound"));
        if (account == null) {
            throw new UserException("UserNotExists");
        }
        return accountRepository.save(accountMapper.toModel(accountDto));
    }

    @Override
    public Account deleteAccount(Long userId) throws UserException {
        Account account = accountRepository.findById(userId)
                .orElseThrow(() -> new UserException("UserNotFoundForDelete"));
        if (account.getId().equals(userId)) {
            account = accountRepository.deleteAccountById(userId)
                    .orElseThrow(() -> new UserException("CannotDeleteUser"));
        }
        return account;
    }

    @Override
    public Account getAccount(Long id) throws UserException {
        return accountRepository.findById(id)
                .orElseThrow(() -> new UserException("UserNotFound"));
    }

    @Override
    public List<Account> getAllAccounts() throws UserException {
        return accountRepository.findAll();
    }
}
